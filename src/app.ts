import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

import mongoose from 'mongoose';

import routes from './routes';

class App {
  public express: express.Application;

  public constructor (DB_PORT: string) {
    this.express = express();
    this.middlewares();
    this.database(DB_PORT);
    this.routes();
  }

  public run (port: string): void {
    this.express.listen(port, () => {
      // eslint-disable-next-line no-console
      console.log(`Server listening on port ${port}`);
    });
  }

  private middlewares (): void {
    this.express.use(morgan('dev'));
    this.express.use(express.json());
    this.express.use(cors);
  }

  private database (DB_PORT: string): void {
    mongoose.connect(DB_PORT, {
      useNewUrlParser: true
    });
  }

  private routes (): void {
    this.express.use(routes);
  }
}

export default App;
