import App from './app';

const DB_HOST = process.env.DB_HOST;
const PORT = process.env.PORT;

const app = new App(DB_HOST);

app.run(PORT);
