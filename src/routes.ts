import { Router } from 'express';

const router = Router();

router.get('/users', (req, res) => {
  res.json({ data: 'Hello World!' });
});

export default router;
